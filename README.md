### 1. GameShell简介
GameShell是一款开源掌机平台，官方提供基于Linux的OS，并提供了一系列的开源组件。

![GameShellPic](SylixOS/doc/picture/GameShell.jpg)

### 2. SylixOSGS简介
SylixOSGS是将SylixOS移植到了GameShell平台，目前支持SD、显示、DPAD、GPU。

![SylixOSGSPic](SylixOS/doc/picture/SylixOSGSPic.png)

### 3. 编译
- 编译需要RealEvo-IDE环境，可以到翼辉信息官网申请体验版。[申请链接](https://www.acoinfo.com/html/experience.php)
- 创建base工程，编译器选择arm，架构选择cortex-a7平台。
- 导入bsp工程，关联base，编译生成bsp_clockwork.bin内核镜像。

### 4. 运行SylixOS
- 将SD卡格式化为FAT分区。
- 将bsp中SylixOS\uboot\u-boot-sunxi-with-spl.bin文件固化到SD卡上，固化方法（在ubuntu下操作），$1替换为实际SD卡在ubuntu下的设备绝对路径名：

```
sudo dd if=u-boot-sunxi-with-spl.bin of=$1 bs=1024 seek=8
```
- 将bsp_clockwork.bin复制到SD卡下，插入到设备上启动。

### 5. 运行3D程序
见SylixOS\driver\gpu\README.md

